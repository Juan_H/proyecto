package logica;

public class CuentaAhorro extends Cuenta {
   

    public CuentaAhorro(int numero, int saldo,String tipo,Cliente cliente) {
        super(numero, saldo, tipo, cliente);
   
    }
	public void consignar(int valor) {
		this.saldo += valor;
	}
	
	public void retirar(int valor) throws Exception {
		if(valor <= this.saldo) {
			this.saldo -= valor;			
		}else {
			throw new Exception("Saldo insuficiente");
		}
	}
	
	public void transferir(Cuenta cuentaDestino, int valor) throws Exception{
		this.retirar(valor);
		cuentaDestino.consignar(valor);
	}
	
	@Override
	public String toString() {
		return this.numero + "\t" + this.saldo + "\t" + this.tipo + "\t" + this.cliente;
	}



   

}
